using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    private Rigidbody2D RbDisparo;
    public float VelocidadBala;

    static int VidasBomba;

    void Start()
    {
        RbDisparo= GetComponent<Rigidbody2D>();
        this.gameObject.transform.eulerAngles = new Vector3(0,0,-90);//girar la bala
    }

    // Update is called once per frame
    void Update()
    {

        RbDisparo.velocity = new Vector2(+VelocidadBala, 0);//Velocidad a a la bala
      
            //RbDisparo.velocity = this.transform.position;
         
        Destroy(this.gameObject, 3f);
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
      
    }
}
