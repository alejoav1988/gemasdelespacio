using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraMovimiento : MonoBehaviour
{

    public Transform TfPersonaje;
    private Vector2 posicionRelativa;

    private Vector2 PosicionCamara;
    void Start()
    {
        posicionRelativa = this.transform.position - TfPersonaje.position;
    }

    void FixedUpdate()
    {


        this.transform.position = new Vector3(TfPersonaje.position.x+posicionRelativa.x, this.transform.position.y,-30);
    }
}
