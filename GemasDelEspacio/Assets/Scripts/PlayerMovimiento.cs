using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovimiento : MonoBehaviour
{

    public float Velocidad = 5f;
    public Rigidbody2D rgPlayer;

    public Transform LugarDisparo;
    public GameObject Disparo;//Bullet

    public float Rafaga = 0.5f;
    public float ProximoDisparo = 0.0f;
    private int Gemas = 0;
    public int Vidas=3;
    public float Sangre = 100.0f;

    void Start()
    {
        rgPlayer = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {



        float mHorizontal = Input.GetAxis("Horizontal");
        float mVertical = Input.GetAxis("Vertical");
        
        Vector2 Movement = new Vector2(mHorizontal, mVertical);

        rgPlayer.velocity = Movement * Velocidad;


        if (Input.GetAxisRaw("Horizontal")>0.1f)

        {
            this.gameObject.transform.eulerAngles = new Vector3(0, -180, 0);//Hacer girar al jugador
            
        }
        if (Input.GetAxisRaw("Horizontal") < -0.1f)

        {
            this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);//Hacer girar al jugador
        }

        if (Input.GetKeyDown(KeyCode.Space) && Time.time > ProximoDisparo)
        {
            ProximoDisparo = Time.time + Rafaga;

            Instantiate(Disparo, this.LugarDisparo.position, this.LugarDisparo.rotation);//Rotacion del personaje
        }

        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bomba")
        {
            Sangre = Sangre - 50.0f;

            if (Sangre<=0)
            {
                if (Vidas <= 0)
                {
                    Destroy(this.gameObject);
                    Debug.Log("Game Over");
                }
                else
                {
                    Vidas--;
                    this.gameObject.transform.position=new Vector3(0, 0, 0);
                    Sangre = 100;
                }

                
            }

            
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {

    }

    private void OnCollisionStay2D(Collision2D collision)
    {



    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "GemaEstrella")
        {
            
            Destroy(collision.gameObject,0.2f);

            Debug.Log("Morci recogio un Gema");

            Gemas++;
        }

        Debug.Log("Total de gemas recogidas: " + Gemas);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {


    }
    private void OnTriggerExit2D(Collider2D collision)
    {

    }
}
